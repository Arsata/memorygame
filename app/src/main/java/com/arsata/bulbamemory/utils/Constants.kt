package com.arsata.bulbamemory.utils

import com.arsata.bulbamemory.R

val DEFAULT_PNG = listOf(
    R.drawable.bulba,
    R.drawable.ivysaur,
    R.drawable.venusaur,
    R.drawable.charmander,
    R.drawable.charmeleon,
    R.drawable.charizard,
    R.drawable.squirtle,
    R.drawable.wartortle,
    R.drawable.blastoise,
    R.drawable.eevee,
    R.drawable.snorlax,
    R.drawable.mew,

)